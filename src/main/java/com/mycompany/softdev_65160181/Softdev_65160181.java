/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.softdev_65160181;

/**
 *
 * @author User
 */
public class Softdev_65160181 {

    public static void main(String[] args) {
        System.out.println("Welcome to OX game");
        System.out.println("Table of Characters");
        System.out.println("A B C");
        System.out.println("D E F");
        System.out.println("G H I");
        
        System.out.println("Turn O");
        System.out.println("Input Character : B");
        System.out.println("A O C");
        System.out.println("D E F");
        System.out.println("G H I");
        
        System.out.println("Turn X");
        System.out.println("Input Character : E");
        System.out.println("A O C");
        System.out.println("D X F");
        System.out.println("G H I");
        
        System.out.println("Turn O");
        System.out.println("Input Character : C");
        System.out.println("A O O");
        System.out.println("D X F");
        System.out.println("G H I");
        
        System.out.println("Turn X");
        System.out.println("Input Character : D");
        System.out.println("A O O");
        System.out.println("X X F");
        System.out.println("G H I");
        
        System.out.println("Turn O");
        System.out.println("Input Character : F");
        System.out.println("A O O");
        System.out.println("X X O");
        System.out.println("G H I");
        
        System.out.println("Turn X");
        System.out.println("Input Character : I");
        System.out.println("A O O");
        System.out.println("X X O");
        System.out.println("G H X");
        
        System.out.println("Turn O");
        System.out.println("Input Character : A");
        System.out.println("O O O");
        System.out.println("X X O");
        System.out.println("G H X");
        System.out.println("O Win !!!");
        
        
        
    }
}
